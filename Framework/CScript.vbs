Set oUFT = CreateObject("QuickTest.Application")
Set oTest = oUFT.Test
oUFT.Visible = True 
oUFT.Launch

scriptdir = CreateObject("Scripting.FileSystemObject").GetParentFolderName(WScript.ScriptFullName)
WScript.Echo scriptdir

''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Get path General
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
PATH_General = Cstr(scriptdir) & "\Library" & "\General"
General = PATH_General&"\GeneralFunctions.vbs"
WScript.Echo General

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Get path Business
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
PATH_Business = Cstr(scriptdir) & "\Library" & "\Business"
Business = PATH_Business&"\BusinessFunctions.vbs"
WScript.Echo Business
''''''''''''''''''''''''''''''''''''''''''''''''''''''''

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'Get path Variables
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
PATH_Config = Cstr(scriptdir) & "\Config"
Variables = PATH_Config&"\Variables.vbs"
WScript.Echo Variables

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'oTest.Settings.Resources.Libraries.Remove General
'oTest.Settings.Resources.Libraries.Remove Business
'oTest.Settings.Resources.Libraries.Remove Variables
oTest.Settings.Resources.Libraries.Add General
oTest.Settings.Resources.Libraries.Add Business
oTest.Settings.Resources.Libraries.Add Variables
'oTest.Actions.Item(2).ObjectRepositories.Remove

oTest.Actions.Count

'''''''''''''''''''''''''''''''''''''''''''
'Get path .Object
'''''''''''''''''''''''''''''''''''''''''''''
PATH_Business = Cstr(scriptdir) & "\Library" & "\Business"
Repository1 = PATH_Business&"\Repository1.tsr"
'#############################################
oTest.Actions.Item(1).ObjectRepositories.RemoveAll
oTest.Actions.Item(1).ObjectRepositories.Add Repository1

'oTest.AddNewAction