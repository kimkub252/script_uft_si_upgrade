﻿'Test_Action = Environment.Value("TestDir")
'WORKSPACE_Action = Replace(CStr(Test_Action),"\Framework\Script\SIUpgradeTest2","\Framework\")
'PATH_Test_Action = Cstr(WORKSPACE_Action) & "TestCase"
'script = PATH_Test_Action&"\TC001_UFT_SIUpgrade"
''Print "TEST_General -->" & script
'
''LoadAndRunAction "C:\BIOS\Framework\Script\SIUpgrade", "TestSuite"
'
'
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''' add file .vbs automatic
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
''Dim sLocal
'Test_General = Environment.Value("TestDir")
'Print "TEST_ --> "&Test_General
'WORKSPACE_General = Replace(CStr(Test_General),"\Framework\Script\SIUpgradeTest2","\Framework\")
'Print "TEST_1 --> "&WORKSPACE_General
'PATH_General = Cstr(WORKSPACE_General) & "Library" & "\General"
'Print "TEST_2 --> "&PATH_General
'General = PATH_General&"\GeneralFunctions.vbs"
'Print "TEST_General --> "&General
''
''WORKSPACE1 = Replace(CStr(TEST_General),"\Framework\Library\General\","\General\")
'Test_Business = Environment.Value("TestDir")
'WORKSPACE_Business = Replace(CStr(Test_Business),"\Framework\Script\SIUpgradeTest2","\Framework\")
'PATH_Business = Cstr(WORKSPACE_Business) & "Library" & "\Business"
'Business = PATH_Business&"\BusinessFunctions.vbs"
'Print "TEST_General --> "&Business
''
''Print "TEST_General --> " & WORKSPACE1
'Test_Config = Environment.Value("TestDir")
'WORKSPACE_Config = Replace(CStr(Test_Config),"\Framework\Script\SIUpgradeTest2","\Framework\")
'PATH_Config = Cstr(WORKSPACE_Config) & "Config"
'Variables = PATH_Config&"\Variables.vbs"
'Print "TEST_General --> "&Variables
'General=PathFinder.Locate ("General\GeneralFunctions1.vbs")
'
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'' add file .vbs automatic
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'
'
'Services.StartTransaction "Script Duration"
'
''Services.StartTransaction "TC001"
''RunAction "Action [TC001_UFT_SIUpgrade]", oneIteration
''Services.EndTransaction "TC001"
'
'Services.EndTransaction "Script Duration"
'
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'' add file .vbs automatic
''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
If sLocal="Dutch" Then
    LoadFunctionLibrary("C:\BIOS\Framework\Library\General\GeneralFunctions.vbs")
Else
    LoadFunctionLibrary("C:\BIOS\Framework\Library\General\GeneralFunctions.vbs")
End If

If sLocal="Dutch1" Then
    LoadFunctionLibrary("C:\BIOS\Framework\Library\Business\BusinessFunctions.vbs")
Else
    LoadFunctionLibrary("C:\BIOS\Framework\Library\Business\BusinessFunctions.vbs")
End If

If sLocal="Dutch2" Then
    LoadFunctionLibrary("C:\BIOS\Framework\Config\Variables.vbs")
Else
    LoadFunctionLibrary("C:\BIOS\Framework\Config\Variables.vbs")
End If

'LoadFunctionLibrary("C:\BIOS\Framework\Library\Business\krungsri.tsr")
'RepositoriesCollection.Add("C:\BIOS\Framework\Library\Business\krungsri.tsr")
LoadAndRunAction "C:\BIOS\Framework\TestCase\GUITest1", "Action1"

'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
' add file .vbs automatic
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''

' Get date time for execute all test

Call FW_StartExecute()
strTag = "Run"

'********************************************************************************
'Initial Start Run
'Create Initial FrameWork	(Define Setup Parameter & Import TestData)
'********************************************************************************
Call FW_CreateInitialFramework()
Call FW_WriteLog("FW_CreateInitialFramework", "OK")
Call FW_WriteLog("FW_CreateInitialFramework", "SUCCESS")
Call FW_WriteLog("StartExecuteTime", startExecuteTime)
If strTag = "" Then
	strTag = "Run"
End If

tag = Split(strTag, ",")
tagArrayCount = UBound(tag)

'Call FW_WriteLog("tag", tag)
'Call FW_WriteLog("tagArrayCount", tagArrayCount)
'Call FW_WriteLog("GetRowCount", DataTable.GlobalSheet.GetRowCount)
Call FW_WriteLog("GetRowCount", DataTable.GetSheet("TestController").GetRowCount)

For dtRow = 1 To DataTable.GetSheet("TestController").GetRowCount Step 1
	DataTable.GetSheet("TestController").SetCurrentRow(dtRow)
	tagAction = UCase(DataTable("Tag", "TestController"))
	testCase = Trim((DataTable("TestCaseNo", "TestController")))
	testName = Trim((DataTable("TestCaseName", "TestController")))
	
	Call FW_WriteLog("Tag", tagAction)
	Call FW_WriteLog("TestCaseNo", testCase)
	Call FW_WriteLog("TestCaseName", testName)
	
	For index = 0 to tagArrayCount Step 1
		If UCase(Trim(tagAction)) = "RUN" Then
			DataTable(RESULT_STATUS, "TestController") = "Run"
		
			'Define Parameter for Create Folder TestCase for Get Snapshot per Test Case
			strResultPathTestCase = strResultPathExecution
			
			'Create Folder before Run
			Call CreateFolder(strResultPathTestCase)
			Call FW_WriteLog("Row Execution " & DataTable.GetSheet("TestController").GetCurrentRow & VbNewLine & "StartTime : " & Now() & VbNewLine & "TestCase : " & testCase & VbNewLine & "TestName : " & testName & VbNewLine & "TestCase Result (CaptureScreenShot) : " & strResultPathTestCase, "")
			
			'Call Function from DataTable Column TestCase
'				Call to Existing Action
'				PATH_Test_Case 
			RUNNING_STEP = 1
			Select Case testCase
				'TC001	
				Case "TC001"						
				'TC002	
				Case "TC002"
				'TC003	
				Case "TC003"
			End Select
		
			'Call Function Stamp Result from Report to Datatable Column 'Desctiption' 			
			Call FW_CreateResult(dtRow)

			Exit For
			
'		ElseIf index = tagArrayCount Then
'			'Create Status not run 
'			DataTable(RESULT_STATUS,dtGlobalSheet) =  "No Run"
'			Exit For
		Else
'			DataTable(RESULT_STATUS, dtGlobalSheet) = tagAction & " <> " & tag(index) & " No Run"
			DataTable(RESULT_STATUS, "TestController") =  "No Run"
		End If
	Next
Next

'Export Result from Datatable
Call FW_StopExecute()
Call FW_WriteLog("FW_StopExecute", "OK")
Call FW_WriteLog("FW_StopExecute", "SUCCESS")
Call FW_WriteLog("StopExecuteTime", stopExecuteTime)

Call FW_ExportResult()
'Print "Execution Completed [" & Now & "]"
Call FW_WriteLog("Execution Completed", "[" & Now & "]")
'FW_ExportReportToDoc()
'FW_StopExecute()
'********************************************************************************
